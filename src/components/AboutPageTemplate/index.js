import React from 'react'
import Helmet from 'react-helmet'
import About from '../About'
import Hero from '../Hero'
import PropTypes from 'prop-types'

const AboutPageTemplate = ({
  title,
  meta_title,
  meta_description,
  about,
}) => {
  return (
    <div>
      <Helmet>
        <title>{meta_title}</title>
        <meta name='description' content={meta_description} />
      </Helmet>
      <Hero title={meta_description} image='/img/approach-banner.png' />
      <About about={about} />
    </div>
  )
}

AboutPageTemplate.propTypes = {
  title: PropTypes.string,
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
  about: PropTypes.shape({
    what_we_do: PropTypes.object,
    team_members: PropTypes.array,
  }),
}

export default AboutPageTemplate
