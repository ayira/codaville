import React from 'react'
import SE0 from '../SEO'
import CourseCard from '../CourseCards'

export default ({ courseBundle }) => {
  const { frontmatter } = courseBundle.markdownRemark
  return (
    <section>
      <SE0
        title={frontmatter.title}
        meta_title={frontmatter.meta_title}
        meta_desc={frontmatter.meta_description}
        slug={frontmatter.slug}
      />
      <div className='hero-body has-background-primary is-fullheight'>
        <div className='coontainer'>
          <p className='title container has-text-weight-bold is-1 has-text-white has-text-centered is-capitalized section'>
            {frontmatter.title} courses
          </p>
          <div className='level'>
            {frontmatter.details.stats.map(item => (
              <div className='level-item has-text-centered'>
                <p className='heading has-text-white'>{item.stat}</p>
                <p className='title'>{item.figure}</p>
              </div>
            ))}
          </div>
          <section className='section box has-background-info'>
            <p className='has-text-centered has-text-white has-text-weight-semibold is-size-5'>
              {frontmatter.meta_description}
            </p>
          </section>
        </div>
      </div>
      <section>
        <CourseCard courseBundle={courseBundle} />
      </section>
    </section>
  )
}
