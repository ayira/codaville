import React from 'react'
import Helmet from 'react-helmet'
import Hero from '../Hero'
import CourseBudle from '../CourseBundle'
import PropTypes from 'prop-types'

import { IconContext } from 'react-icons'
import { FaRegPlayCircle } from 'react-icons/fa'

const HomePageTemplate = ({
  meta_title,
  meta_description,
  promo_benefits,
  courses_promo,
  intro,
}) => (
  <div>
    <Helmet>
      <title>{meta_title}</title>
      <meta name='description' content={meta_description} />
    </Helmet>
    <Hero image='/img/codakid-logo.png' title={intro.blurbs[0].title} isBackgroundImage={false} />
    <section className='intro'>
      <div className='container is-fluid'>
        <div className='columns has-text-centered'>
          <div className='column is-2'>
            <figure className='image'>
              <img src={intro.blurbs[0].main_image} />
            </figure>
          </div>
          <div className='column'>
            <div className='section'>
              <p className='title has-text-primary box'>{intro.blurbs[0].sub_caption}</p>
            </div>
          </div>
          <div className='column is-2'>
            <figure className='image'>
              <img src={intro.blurbs[1].main_image} />
            </figure>
          </div>
        </div>
        <div className='columns'>
          <div className='column is-half'>
            <div className='card has-background-primary'>
              <div className='card-content'>
                <p className='title has-text-centered'>{intro.blurbs[1].title}</p>
                <p className='title is-5 has-text-centered'>{intro.blurbs[1].caption}</p>
                <p className='subtitle has-text-centered'>{intro.blurbs[1].sub_caption}</p>
              </div>
            </div>
          </div>
          <div className='column is-half'>
            <div className='card has-background-warning'>
              <div className='card-content'>
                <p className='title has-text-centered'>{intro.blurbs[2].title}</p>
                <p className='title is-5 has-text-centered'>{intro.blurbs[2].caption}</p>
                <p className='subtitle has-text-centered'>{intro.blurbs[2].sub_caption}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='section has-text-centered'>
        <p className='has-text-primary is-inline-block has-text-weight-bold is-size-5 box'>
          {intro.sub_caption}
        </p>
        <br />
        <p className='has-text-primary is-inline-block has-text-weight-semibold is-size-5 box'>
          {intro.caption}
        </p>
      </div>
    </section>
    <div
      className='has-text-centered title has-text-white choose-us-banner'
      style={{padding: '20px 0', marginBottom: '0px'}}
    >
      <span>{intro.video.title}</span>
      <IconContext.Provider value={{size: '1.5em', style: {verticalAlign: 'middle', marginLeft: '15px'}}}>
        <FaRegPlayCircle />
      </IconContext.Provider>
      <span style={{paddingLeft: '15px'}}>Play video</span>
    </div>
    <section className='choose-us-banner'>
      <div className='container is-fluid'>
        <div className='columns is-gapless'>
          <div className='column is-one-third'>
            <div className='card'>
              <div className='card-image'>
                <figure className='image is-4by3'>
                  <img src='/img/1.jpg' alt='Placeholder image' />
                </figure>
              </div>
              <div className='card-content'>
                <div className='media'>
                  <div className='media-content has-text-centered'>
                    <p className='title is-4 '>
                      Prepare Your Kid For The Digital Age!
                    </p>
                  </div>
                </div>
                <div className='content'>
                  CodaKid students learn professional computer coding languages such as Python, JavaScript, and
                  Java while using the same tools used at companies like Google, Facebook, and Intel.
                </div>
              </div>
            </div>
          </div>
          <div className='column is-one-third'>
            <div className='card'>
              <div className='card-image'>
                <figure className='image is-4by3'>
                  <img src='/img/3.jpg' alt='Placeholder image' />
                </figure>
              </div>
              <div className='card-content'>
                <div className='media'>
                  <div className='media-content has-text-centered'>
                    <p className='title is-4 '>
                      Make Learning A Fun Game For Your Kid!
                    </p>
                  </div>
                </div>
                <div className='content'>
                  Make professional grade games and software while earning badges and being awarded points!
                  Kids rave about CodaKid!
                </div>
              </div>
            </div>
          </div>
          <div className='column is-one-third'>
            <div className='card'>
              <div className='card-image'>
                <figure className='image is-4by3'>
                  <img src='/img/2.jpg' alt='Placeholder image' />
                </figure>
              </div>
              <div className='card-content'>
                <div className='media'>
                  <div className='media-content has-text-centered'>
                    <p className='title is-4 '>
                      Give Your Child The Mentors They Need For Success!
                    </p>
                  </div>
                </div>
                <div className='content'>
                  CodaKid's online teachers are experienced computer software developers, designers,
                  and educators who double as mentors make coding fun and engaging for kids.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='container is-fluid section'>
        <p className='has-text-centered'>{intro.promo_langs.caption}</p>
        <div className='columns'>
          {intro.promo_langs.images.map(item => (
            <div key={item.image} className='column' >
              <img src={item.image} />
            </div>
          ))}
        </div>
      </div>
    </section>
    <section className='section stats-banner'>
      <div className='container is-fluid'>
        <div className='columns is-gapless is-vcentered'>
          <div className='column is-one-third'>
            <img src={intro.promo_banner.image} className='image' />
          </div>
          <div className='column is-two-third has-background-info'>
            <p className='title section has-text-centered has-text-white'>{intro.promo_banner.title}</p>
            {intro.promo_banner.stats.map(stat => {
              return (
                <div key={stat.caption} className='is-inline section has-text-white'>
                  <span>{stat.figure} {stat.caption} | </span>
                </div>
              )
            })}
            <p className='subtitle has-text-centered has-text-white section'>{intro.promo_banner.caption}</p>
            <div className='has-text-centered'>
              <button className='button is-large is-uppercase is-radiusless'>
                try for free
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className='section generic-banner'>
      <div className='container is-fluid'>
        <div className='columns'>
          {promo_benefits.map(benefit => (
            <div key={benefit.title} className='column is-one-fifth'>
              <figure className='is-flex is-horizontal-img-center'>
                <img src={benefit.image} className='image is-64x64' />
              </figure>
              <p className='title is-6 has-text-centered'>{benefit.title}</p>
              <p className='has-text-centered has-text-weight-semibold'>{benefit.content}</p>
            </div>
          ))}
        </div>
      </div>
    </section>
    <section>
      <div className=''>
        <p className='title has-text-centered' style={{marginBottom: '0'}}>What's Included</p>
        <CourseBudle />
      </div>
    </section>
  </div>
)

HomePageTemplate.propTypes = {
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
  offerings: PropTypes.shape({
    blurbs: PropTypes.array,
  }),

}

export default HomePageTemplate
