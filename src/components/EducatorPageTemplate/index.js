import React from 'react'
import Helmet from 'react-helmet'
import Educator from '../Educator'
import Hero from '../Hero'
import PropTypes from 'prop-types'

const EducatorPageTemplate = ({
  title,
  meta_title,
  meta_description,
  educator,
}) => {
  return (
    <div>
      <Helmet>
        <title>{meta_title}</title>
        <meta name='description' content={meta_description} />
      </Helmet>
      <Hero
        title={title}
        subtitle={meta_description}
        image='/img/schools_header.jpg'
      />
      <Educator educator={educator} />
    </div>
  )
}

EducatorPageTemplate.propTypes = {
  title: PropTypes.string,
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
  educator: PropTypes.shape({
    courses: PropTypes.object,
    feature_banner: PropTypes.object,
    language_banner: PropTypes.object,
    promo_banner: PropTypes.object,
    inquire_banner: PropTypes.object,
  }),
}

export default EducatorPageTemplate
