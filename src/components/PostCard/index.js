import React from 'react'
import {Link} from 'gatsby'

const PostCard = ({posts}) => {
  return (
    <div className='container'>
      <div className='columns is-multiline'>
        {posts
          .filter(post => post.node.frontmatter.templateKey === 'article-page')
          .map(({node: post}) => (
            <div className='column is-one-third' key={post.id}>
              <div className='card'>
                <div className='card-image' style={{backgroundColor: '#783f7d45'}}>
                  <figure className='image'>
                    <img src={post.frontmatter.cover} />
                  </figure>
                </div>
                <div className='card-content'>
                  <div className='level'>
                    <Link className='has-text-primary' to={post.fields.slug}>
                      {post.frontmatter.title}
                    </Link>
                    <small className='has-text-info is-uppercase'>{post.frontmatter.date}</small>
                  </div>
                  <p className='section'>
                    {post.excerpt}
                  </p>
                </div>
                <div className='card-footer' style={{border: 'none'}}>
                  <Link className='button is-radiusless is-small is-info' to={post.fields.slug}>
                    Keep Reading →
                  </Link>
                </div>
              </div>
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default PostCard
