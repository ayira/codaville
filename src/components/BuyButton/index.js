import React from 'react'

const BuyButton = React.memo(({course}) => {
  let choosenImgSrc = course.image

  return (
    <div>
      <a
        id={course.id}
        href='#'
        className='snipcart-add-item button is-radiusless is-centered is-large is-primary is-uppercase'
        data-item-id={course.id}
        data-item-price={course.price}
        data-item-image={choosenImgSrc}
        data-item-name={course.plan}
        data-item-description={course.description}
        data-item-url={'https://prod-codaville.netlify.com/' + course.path}>
        add to cart for ${course.price}
      </a>
    </div>
  )
})

export default BuyButton
