import React from 'react'
import {Link, graphql, StaticQuery} from 'gatsby'
import SearchBox from '../SearchBox'

const NavBar = ({toggleNavbar, isActive}) => (
  <StaticQuery
    query={graphql`
            query SearchIndexQuery {
                siteSearchIndex {
                    index
                }
            }
        `}
    render={data => (
      <nav className='navbar is-fixed-top' aria-label='main navigation'>
        <div className='navbar-brand'>
          <Link to='/' className='navbar-item'>
            <strong className='title has-text-info is-uppercase'>Codaville</strong>
          </Link>
          <button
            className={`button is-white navbar-burger ${isActive ? 'is-active' : ''}`}
            data-target='navMenu'
            onClick={toggleNavbar}
          >
            <span />
            <span />
            <span />
          </button>
        </div>
        <div className={`navbar-menu has-background-info ${isActive ? 'is-active' : ''}`} id='navMenu'>
          <div className='navbar-start'>
            <Link className='navbar-item' to='/about' activeClassName='is-active'>
                About
            </Link>
            <Link className='navbar-item' to='/blog' activeClassName='is-active'>
                Blog
            </Link>
            <Link className='navbar-item' to='educator' activeClassName='is-active'>
                Educators
            </Link>
            <Link className='navbar-item' to='#' activeClassName='is-active'>
                Videos
            </Link>
            <div className='navbar-item has-dropdown is-hoverable'>
              <Link className='navbar-link' to=''>
                Camps
              </Link>
              <div className='navbar-dropdown'>
                <a className='navbar-item' to='#'>
                  Winter
                </a>
                <a className='navbar-item' to='#'>
                  Summer
                </a>
                <a className='navbar-item' to='#'>
                  Classes
                </a>
              </div>
            </div>
          </div>

          <div className='navbar-end'>
            <SearchBox searchIndex={data.siteSearchIndex.index} />
            <div className='navbar-item'>
              <div className='field is-grouped'>
                <p className='control'>
                  <Link
                    className='button is-primary is-large'
                    to='/pricing'>
                      Buy Course
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )}
  />
)

export default NavBar
