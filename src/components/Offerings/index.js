import React from 'react'
import PropTypes from 'prop-types'

const Offerings = ({ gridItems }) => (
  <div className='columns is-multiline is-centered'>
    {gridItems.map(item => (
      <div key={item.image} className='column'>
        <div className='card'>
          <div className='card-image'>
            <figure className='image is-4by3'>
              <img src={item.image} alt='Placeholder image' />
            </figure>
          </div>
          <div className='card-content'>
            <div className='media'>
              <div className='media-content has-text-centered'>
                <p className='title is-4 '>John Smith</p>
              </div>
            </div>
            <div className='content'>
              {item.exerpt}
            </div>
          </div>
        </div>
      </div>
    ))}
  </div>
)

Offerings.propTypes = {
  gridItems: PropTypes.arrayOf(
    PropTypes.shape({
      image: PropTypes.string,
      text: PropTypes.string,
    })
  ),
}

export default Offerings
