import React from 'react'
import PropTypes from 'prop-types'

const Educator = ({ educator }) => (
  <div>
    <div className='has-text-centered has-background-info'>
      <p className='section title has-text-white is-1 is-uppercase'>
        educator's courses overview
      </p>
    </div>
    <section className='section generic-banner'>
      <div className='container is-fluid'>
        <div className='columns is-vcentered is-centered is-multiline'>
          {educator.courses.map(course => (
            <div className='column is-one-third'>
              <div className='card'>
                <div className='card-header'>
                  <div className='card-header-title is-centered'>
                    <p className='title is-5'>
                      {course.title}
                    </p>
                  </div>
                </div>
                <div className='card-image' style={{backgroundColor: '#8bc34a'}}>
                  <figure className='image'>
                    <img src={course.image} />
                  </figure>
                </div>
                <div className='card-content'>
                  <p>
                    {course.description}
                  </p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
    <section className='is-marginless is-paddingless'>
      <div className='columns is-centered is-gapless is-multiline'>
        {educator.feature_banner.map(feature => {
          const COLORS = [
            'has-background-primary',
            'has-background-info',
            'has-background-warning',
          ]
          return (
            <div className={`column is-one-third ${COLORS[Math.floor(Math.random() * COLORS.length)]}`}>
              <div className='section'>
                <figure className='is-flex is-horizontal-img-center'>
                  <img src={feature.image} className='image is-128x128' />
                </figure>
                <div className='section title has-text-centered has-text-white'>
                  <p>{feature.caption}</p>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    </section>
    <div
      style={{
        backgroundImage: `url(${educator.inquire_banner[0].image})`,
        backgroundSize: 'cover',
        height: '500px',
      }}
      className='has-text-centered columns is-vcentered is-marginless'
    >
      <div className='section column is-12'>
        <p className='title has-text-white'>{educator.inquire_banner[0].caption}</p>
        <button className='button is-large is-primary'>Request Quote</button>
      </div>
    </div>
    <section className='choose-us-banner'>
      <div className='container is-fluid section'>
        <p className='has-text-centered'>{educator.language_banner.caption}</p>
        <div className='columns'>
          {educator.language_banner.images.map(item => (
            <div key={item.image} className='column' >
              <img src={item.image} />
            </div>
          ))}
        </div>
      </div>
    </section>
    <section className='section'>
      <div className='container is-fluid'>
        {educator.promo_banner.promo_blurbs.map(blurb => (
          <div className='columns is-vcentered is-gapless'>
            <div className='column is-half has-background-grey-lighter'>
              <div className='section'>
                <figure className='is-flex is-horizontal-img-center'>
                  <img src={blurb.icon} className='image is-128x128' />
                </figure>
                <div className='section has-text-centered has-text-white'>
                  <p className='title'>{blurb.title}</p>
                  <p className='has-text-black'>{blurb.description}</p>
                </div>
              </div>
            </div>
            <div className='column is-half'>
              <figure className='image'>
                <img src={blurb.image} />
              </figure>
            </div>
          </div>
        ))}
      </div>
    </section>
    <div className='has-background-warning'>
      <div className='section has-text-centered'>
        <p className='title has-text-white'>SECURE AND FERPA COMPLIANT</p>
        <figure className='is-flex is-horizontal-img-center section'>
          <img src={educator.promo_banner.image} className='image' />
        </figure>
        <div className='section has-text-centered is-uppercase has-background-info box is-inline-block'>
          <p className='subtitle has-text-white'>{educator.promo_banner.caption}</p>
          <p className='subtitle has-text-white' >{educator.promo_banner.subcaption}</p>
        </div>
      </div>
    </div>
    <div
      style={{
        backgroundImage: `url(${educator.inquire_banner[1].image})`,
        backgroundSize: 'cover',
        height: '500px',
      }}
      className='has-text-centered columns is-vcentered'
    >
      <div className='section column is-12'>
        <p className='title has-text-white'>{educator.inquire_banner[1].caption}</p>
        <button className='button is-large is-primary'>Request Quote</button>
      </div>
    </div>
  </div>
)

Educator.propTypes = {
  educator: PropTypes.shape({
    courses: PropTypes.object,
    feature_banner: PropTypes.object,
    language_banner: PropTypes.object,
    promo_banner: PropTypes.object,
    inquire_banner: PropTypes.object,
  }),
}

export default Educator
