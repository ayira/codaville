import React from 'react'

const Hero = ({
  image,
  title,
  subtitle = null,
  isBackgroundImage = true,
  className = 'hero is-medium',
}) => {
  if (isBackgroundImage) {
    return (
      <section
        className={className}
        style={{
          backgroundImage: `url(${image})`,
          backgroundSize: 'cover',
          backgroundAttachment: 'fixed',
        }}
      >
        <div className='hero-body' style={{backgroundColor: '#8bc34a96'}}>
          <div className='coontainer'>
            <p className='title container has-text-weight-bold is-1 has-text-white has-text-centered section'>
              {title}
            </p>
            <h2 className='subtitle has-text-weight-semibold has-text-centered'>
              {subtitle}
            </h2>
          </div>
        </div>
      </section>)
  } else {
    return (
      <section className='hero is-bold'>
        <div className='hero-foot'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-one-third'>
                <div className='section'>
                  <figure className='image'>
                    <img src={image} />
                  </figure>
                </div>
              </div>
              <div className='column'>
                <p className='title has-text-info has-text-centered section'>{title}</p>
              </div>
            </div>
          </div>
        </div>
      </section>)
  }
}

export default Hero
