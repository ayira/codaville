import React from 'react'
import { IconContext } from 'react-icons'
import { FaFacebookF, FaTwitter, FaGooglePlusG, FaPinterestP } from 'react-icons/fa'

import config from '../../../data/config'

// library.add(faIgloo)

const Footer = () => {
  const fontProps = {size: '1.5em'}
  return (
    <div>
      <section className='section choose-us-banner'>
        <div className='container is-fluid'>
          <div className='columns'>
            <div className='column is-one-third section has-text-centered'>
              <p className='title has-text-white'>
              EARN AWESOME BADGES
              </p>
              <p className='content has-text-white section'>
                Get exciting badges when you complete each course. Finish all of the courses to earn them all!
              </p>
              <button className='button is-large is-primary'>TRY FOR FREE</button>
            </div>
            <div className='column is-two-third'>
              <figure className='image'>
                <img src='/img/badges.png' />
              </figure>
            </div>
          </div>
        </div>
      </section>
      <div className='has-background-warning'>
        <div className='front-footer-logo'>
          <div className='front_footer_logo_top' />
          <div className='front_footer_logo_right' />
          <div className='section'>
            <figure>
              <img src='/img/codakid-logo.png' />
            </figure>
            <div className='level section'>
              <a className='level-item has-text-white-ter' href='#' target='_blank'>
                <IconContext.Provider value={fontProps}>
                  <FaTwitter />
                </IconContext.Provider>
              </a>
              <a className='level-item has-text-white-ter' href='#'>
                <IconContext.Provider value={fontProps}>
                  <FaFacebookF />
                </IconContext.Provider>
              </a>
              <a className='level-item has-text-white-ter' href='#'>
                <IconContext.Provider value={fontProps}>
                  <FaGooglePlusG />
                </IconContext.Provider>
              </a>
              <a className='level-item has-text-white-ter' href='#'>
                <IconContext.Provider value={fontProps}>
                  <FaPinterestP />
                </IconContext.Provider>
              </a>
            </div>
          </div>
        </div>
        <div className='front_footer_links'>
          <ul className='front_footer_menu section'>
            <li>
              <a href='#' className='has-text-white-ter is-size-5'>About Us</a>
            </li>
            <li>
              <a href='#' className='has-text-white-ter is-size-5'>Our Approach</a>
            </li>
            <li><a href='#' className='has-text-white-ter is-size-5'>Affiliates</a></li>
          </ul>
          <ul className='front_footer_menu section'>
            <li><a href='#' className='has-text-white-ter is-size-5'>Contact Us</a></li>
            <li><a href='#' className='has-text-white-ter is-size-5'>Educators</a></li>
            <li><a href='#' className='has-text-white-ter is-size-5'>Privacy Policy</a></li>
          </ul>
        </div>
        <div className='front_footer_contact'>
          <div className='front_footer_contact_left' />
          <div className='front_footer_contact_right' />
          <div className='front_footer_contact_content section has-text-white-ter'>
            <p className='title has-text-centered'>Codaville</p>
            <div className='content has-text-centered'>
            4254 North Brown Avenue <br />
            Scottsdale, AZ 85251 USA <br />
            480.405.2734 tel<br />
            hello@codaville.com<br />
            </div>
          </div>
          <div className='front_footer_contact_shape' />
        </div>
        <div className='front_footer_contact_after' />
        <footer className='footer has-background-info'>
          <div className='container'>
            <div className='content has-text-centered has-text-white'>
              <p>
                {config.copyright}
              </p>
              <p>
              Codaville courses and camps are not official Minecraft® products. They are not approved by, or associated
              with Mojang®. NOT AN OFFICIAL MINECRAFT PRODUCT. NOT APPROVED BY OR ASSOCIATED WITH MOJANG.
              </p>
            </div>
          </div>
        </footer>
      </div>
    </div>
  )
}

export default Footer
