import React from 'react'

export default ({courseBundle}) => {
  return (
    <div key={courseBundle.title}>
      <div className='title is-5 has-text-centered is-capitalized'>
        <p className='box is-inline-block'>{courseBundle.title}</p>
      </div>
      <div className='container is-fluid columns is-centered is-multiline'>
        {courseBundle.details.courses.map(course => (
          <div key={course.title} className='column is-one-quarter'>
            <div className='card'>
              <div className='card-image'>
                <figure className='image'>
                  <img src={course.cover} alt={course.title} />
                </figure>
              </div>
              <div className='card-content'>
                <p className='title is-6'>
                  {course.title}
                </p>
                <progress className='progress is-small' value='0' max='100'>0%</progress>
              </div>
            </div>
          </div>
        ))}

      </div>
    </div>
  )
}
