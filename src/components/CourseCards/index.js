import React from 'react'

export default ({courseBundle}) => {
  const {frontmatter} = courseBundle.markdownRemark
  return (
    <div>
      {frontmatter.details.courses.map((course, index) => {
        if (index % 2 === 0) {
          return (
            <div className='columns is-multiline is-gapless is-marginless is-vcentered'>
              <div className='column is-half has-text-centered'>
                <p className='title has-text-centered'>{course.title}</p>
                <p className='section has-text-centered'>{course.description}</p>
                <div className='is-centered'>
                  <button className='button is-uppercase is-static'>
                    {course.difficulty}
                  </button>
                </div>
              </div>
              <div className='column is-half'>
                <figure className='image'>
                  <img src={course.cover} />
                </figure>
              </div>
            </div>
          )
        } else {
          return (
            <div className='columns is-multiline is-gapless is-marginless is-vcentered'>
              <div className='column is-half'>
                <figure className='image'>
                  <img src={course.cover} />
                </figure>
              </div>
              <div className='column is-half has-text-centered'>
                <p className='title has-text-centered'>{course.title}</p>
                <p className='section has-text-centered'>{course.description}</p>
                <div className='is-centered'>
                  <button className='button is-uppercase is-static'>
                    {course.difficulty}
                  </button>
                </div>
              </div>
            </div>
          )
        }
      })}
    </div>
  )
}
