---
templateKey: article-page
slug: hour-of-Code-2018-roblox-coding-with-lua
date: 2018-09-29T03:55:49.370Z
cover: /img/blog/AdvancedObbiesPosterLong-297x208.png
header_cover: /img/blog/AdvancedObbiesPosterLong.png
title: "Hour of Code 2018 | Roblox Coding with Lua"
meta_title: "Hour of Code 2018 | Roblox Coding with Lua"
meta_description: "Hour of Code 2018 | Roblox Coding with Lua | Free Activity Activity Length: 5 hours |..."
tags:
  - coding
  - robox
  - lua
---
Hour of Code is nearly here, and in this free guide called Hour of Code 2018 | Roblox Coding with Lua, we share a series of 
free activities on one of our favorite kids coding and game development platforms – Roblox Studio.

 

In this Hour of Code activity, CodaKid will show you how to make an awesome Obby style game using the power of Lua 
programming and the powerful game creation platform Roblox Studio.

 

“Obby”, short for Obstacle, is a fun obstacle course-style game that the kids will create using their own creativity and a 
bit of Lua code. It is rated G and suitable for all ages 10+.

 

For those teachers and students who may have only had experience with visual block coding, please keep in mind that 
text-based Lua coding is more advanced, but can still be handled without much difficulty if you pay careful attention to 
the tutorials and double check your work.

 

Important Note for  Educators and Schools: The tutorials themselves are streamed from YouTube, but the assets you will need 
in order to run Roblox Studio editor may require your IT department whitelist a few URLs. This should only take your IT 
department a few minutes and instructions may be found here.

 

These tutorials provide a very clear explanation of everything that your students will need to do. You can either broadcast
the videos from a projector or smartboard and have all of the students work on them together, or our preferred method is 
to have the kids use headsets and progress at their own pace.

 

If you have any questions or issues, please drop us a note at hello at codakid dot com and we’ll do our best to help you 
out!

 

Here is a fun intro trailer of CodaKid’s free Obby tutorial that is presented in the series of instructional videos below. 
We sincerely hope that you enjoy them as much as we had fun making them!
