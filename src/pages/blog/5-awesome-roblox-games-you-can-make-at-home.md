---
templateKey: article-page
slug: 5-awesome-roblox-games-you-can-make-at-home
date: 2018-09-29T03:55:49.370Z
cover: /img/blog/KeyArt-GR-Swordburst-Flood-297x208.png
header_cover: /img/blog/KeyArt-GR-Swordburst-Flood.png
title: "5 Awesome Roblox Games You Can Make At Home"
meta_title: "5 Awesome Roblox Games You Can Make At Home"
meta_description: "Look out gaming world – Roblox is on a tear. Exploding in popularity over the last few..."
tags:
  - game
  - robox
---
Look out gaming world – Roblox is on a tear. Exploding in popularity over the last few years, Roblox now boasts over 64M
monthly players, and is poised to overtake Minecraft as the #1 sandbox style game in the world.

While it’s fun to play other people’s games on the Roblox platform, increasing numbers of players are learning to create 
their own games on Roblox Studio, Roblox’s unique game engine. Developing games on Roblox is something that anyone can 
learn, it’s free, and in fact there have been numerous articles about even teenagers who have become millionaires making 
games for the Roblox marketplace.

In this blog, we are going to discuss 5 Roblox games that even complete beginners to game design and coding can make at 
home. As a special bonus, we’ve even included a link to our step-by-step Obby video tutorial that will teach you how to 
make your own custom obstacle course using Roblox Studio and the Lua programming language.

1. Obby
 
The first on our list of Roblox games you can make at home is an “Obby.” An Obby or Obstacle Course is a great beginner 
game that will teach you the basics of Roblox Studio and the Lua scripting language. An Obby presents the player with a 3D 
Mario style game in which the player must proceed through a series of obstacles without perishing. Obstacle courses present 
a number of micro-challenges that get progressively more difficult as the player advances through the course.

In an Obby you will learn how to use game elements such as Jumps, Kill blocks, Challenges, Checkpoints, and Level 
Advancement. In the Roblox Level editor you will need to learn about parts, scripts, materials, and transform tools in 
order to build a professional quality Obby.

In order to make an Obby work properly, you will need to write some scripts using the Lua scripting language. While it may 
seem intimidating at first, Lua is an elegant, easy-to-learn programming language, and there many tools and resources that 
can help you master it.

One concept that you will see in Lua while make your Obby is a conditional statement. In the example below, we tell the 
computer to set the player’s health to zero (thereby killing the player) if the player touches a block.
