---
templateKey: course-page
title: Roblox Coding
meta_title: Roblox Coding | Codaville
meta_description: CodaKid's Roblox coding courses for ages 8+ provide the perfect way to learn Lua
  programming and game design with Roblox Studio. Kids make amazing professional quality games that they 
  can play with friends and family, or even sell on Roblox marketplace.
slug: roblox-coding
details:
  benefits_promo:
    - image:
      content:
  benefits_promo_alt:
    - title:
      content: 
      image:
  stats:
    - figure: 9
      stat: courses
    - figure: 75
      stat: quests
    - figure: 137
      stat: challenges
  courses: 
    - title: "Roblox Game Development: Obbies"
      cover: /img/page_courses/roblox-coding/courses/roblox-game-development-obbies.jpg
      difficulty: intro
      description: In this beginner Roblox coding course, students learn the basics of the Roblox Studio
        editor and use real Lua programming to make a professional quality Obstacle Course or "Obby" 
        game. Students master variables, functions and events.
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Roblox Game Development: Adventure Maps"
      cover: /img/page_courses/roblox-coding/courses/roblox-game-development-adventure-maps.jpg
      difficulty: beg
      description: In this popular Roblox coding course, students use the Roblox Studio editor and Lua
        programming to build their very own Adventure Map. We cover key concepts such as conditionals 
        and variables.
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Roblox Game Development: Racing Maps"
      cover: /img/page_courses/roblox-coding/courses/roblox-game-development-racing-maps.jpg
      difficulty: int
      description: In this heart-pumping Roblox course, kids build exciting multiplayer racing games
        with Roblox Studio and the Lua programming language. We cover key concepts such as loops, 
        conditionals, UI, and arrays.
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Roblox Game Development: Advanced Obbies"
      cover: /img/page_courses/roblox-coding/courses/roblox-game-development-advanced-obbies.jpg
      difficulty: int
      description: In this advanced Roblox game development course, students take Obbies to the next
        level with advanced game dynamics. Students deepen their knowledge of functions, events, and 
        more!
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Roblox Game Development: Infinite Runner"
      cover: /img/page_courses/roblox-coding/courses/roblox-game-development-infinite-runner.jpg
      difficulty: int
      description: In this Intermediate level Roblox coding course, kids learn real Lua programming while
        making a fun Infinite Runner style game! Students gain more advanced knowledge of loops and 
        conditionals in this course.
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Roblox Game Development: Dodgeball Arena"
      cover: /img/page_courses/roblox-coding/courses/roblox-game-development-dodgeball-arena.jpg
      difficulty: int
      description: In this advanced Roblox scripting course, students create a Dodgeball arena using 
        Roblox Studio and code the game dynamics with Lua. Students deepen their understanding of loops. 
        conditionals, and collision detection.
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Roblox Game Development: Battle Royale"
      cover: /img/page_courses/roblox-coding/courses/roblox-game-development-battle-royale.png
      difficulty: adv
      description: In this Roblox game development course, students create a Fortnite-style Battle
        Royale Game with custom weapons of their own design! Students will learn advanced concepts 
        covering animation, for loops, global scripts, and client/server events.
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Roblox Game Development: Tycoon Game"
      cover: /img/page_courses/roblox-coding/courses/roblox-game-development-tycoon-game.jpg
      difficulty: adv
      description: In this advanced Roblox game development course, students create their own Tycoon
        Game where players can get rich and compete in building their own Roblox Factory Empire! 
        Students will learn advanced concepts covering inheritance, workspace hierarchy, object values, 
        and type casting.
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Roblox Game Development: Speed Simulator"
      cover: /img/page_courses/roblox-coding/courses/speed_simulator.png
      difficulty: adv
      description: In this advanced Roblox game development course, students create an addictive Speed
        Simulator game where players can race their friends and trade steps for new trails! Students 
        gain an understanding of Client and Server interaction, loops, conditionals, and UI.
      chapters:
        - image:
          title:
      requirements:
        - title:
          image:
          points:
---