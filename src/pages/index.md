---
templateKey: 'home-page'
title: Codaville
meta_title: Online coding For Kids | Codaville
meta_description: >
  Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
  ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam
  venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis
  consectetur purus sit amet fermentum.
promo_badges:
  image: /img/badges.png
  caption: EARN AWESOME BADGES
  content: Get exciting badges when you complete each course. Finish all of the courses to earn them all!
courses_promo:
  - title: 9 Roblox Coding Courses
    courses:
      - image: /img/courses/ObbiesPoster.png
        title: "ROBLOX GAME DEVELOPMENT: OBBIES"
        caption: intro
        sub_caption: 
      - image: /img/courses/AdventurePoster.png
        title: "ROBLOX GAME DEVELOPMENT: ADVENTURE MAPS"
        caption: beg
        sub_caption: 
      - image: /img/courses/RacingPoster.png
        title: "ROBLOX GAME DEVELOPMENT: RACING MAPS"
        caption: int
        sub_caption: 
      - image: /img/courses/AdvancedObbiesPoster.png
        title: "ROBLOX GAME DEVELOPMENT: ADVANCED OBBIES"
        caption: int
        sub_caption: 
      - image: /img/courses/InfiniteRunnerPoster-1.png
        title: "ROBLOX GAME DEVELOPMENT: INFINITE RUNNER"
        caption: int
        sub_caption: 
      - image: /img/courses/DodgeballPoster.png
        title: "ROBLOX GAME DEVELOPMENT: DODGEBALL ARENA"
        caption: int
        sub_caption:
      - image: /img/courses/roblox-game-development-battle-royale.png
        title: "ROBLOX GAME DEVELOPMENT: BATTLE ROYALE"
        caption: adv
        sub_caption: new
      - image: /img/courses/TycoonPoster-1.png
        title: "ROBLOX GAME DEVELOPMENT: TYCOON GAME"
        caption: adv
        sub_caption: new
      - image: /img/courses/SpeedSimulator-thumb.png
        title: "ROBLOX GAME DEVELOPMENT: SPEED SIMULATOR"
        caption: adv
        sub_caption: new
  - title: 14 Minecraft Coding Courses
    courses:
      - image: /img/courses/MCE_Course_Poster_600x600.png
        title: MOD CREATION ESSENTIALS
        caption: intro
        sub_caption: new
      - image: /img/courses/forge_your_sword_600x600.png
        title: "MOD CREATION 1: FORGE YOUR SWORD"
        caption: beg
        sub_caption: new
      - image: /img/courses/PO_Course_Poster_600x600-1.png
        title: "MOD CREATION 1: POWER  ORE"
        caption: beg
        sub_caption: new
      - image: /img/courses/CaC_CoursePoster_600x600.png
        title: "MOD CREATION 1: CREATE A CREATURE"
        caption: beg
        sub_caption: new
      - image: /img/courses/LB_Course_Poster_600x600.png
        title: "MOD CREATION 1: LUCKY BLOCK"
        caption: int
        sub_caption: new
      - image: /img/courses/magic_armor_600x600.png
        title: "MOD CREATION 1: MAGIC ARMOR"
        caption:
        sub_caption:
      - image: /img/courses/brand_new_biomes_600x600.png
        title: "MOD CREATION 1: BRAND NEW BIOMES"
        caption: new
        sub_caption: int
      - image: /img/courses/EW_CoursePoster.png
        title: "MOD CREATION 1:  EPIC WEAPONS"
        caption: new
        sub_caption: int
      - image: /img/courses/BaB_CoursePoster.png
        title: "MOD CREATION 1: BUILD AND BOOM!"
        caption: new
        sub_caption: Adv
      - image: /img/courses/house_in_a_box_600x600.png
        title: "MOD CREATION 1: HOUSE IN A BOX"
        caption: new
        sub_caption: Adv
      - image: /img/courses/infinite-items-thumb.png
        title: "MOD CREATION 1: INFINITE ITEMS 1.12"
        caption: new
        sub_caption: Adv
      - image: /img/courses/ai_and_fireballs_thumb.jpg
        title: "MOD CREATION 1: ARTIFICIAL INTELLIGENCE AND FIREBALLS"
        caption: new
        sub_caption: Adv
      - image: /img/courses/BoxArtCropped.png
        title: MOD CREATION 1 WITH MINECRAFT 1.8
        caption: 
        sub_caption: beg
      - image: /img/courses/DimensionsCropped.png
        title: MOD CREATION 2 WITH MINECRAFT 1.8
        caption: 
        sub_caption: int
promo_benefits:
  - image: /img/benefits/1.png
    title: Professional Tools and Languages
    content: CodaKid students learn professional languages such as Python, JavaScript, etc.
  - image: /img/benefits/2.png
    title: Build Real Games and Software
    content: Make professional grade games and software using cutting edge technology.
  - image: /img/benefits/3.png
    title: Fast & Helpful In-person Support
    content: Our friendly teachers will help your child through messaging and screen share.
  - image: /img/benefits/4.png
    title: Future Innovators Are Born Here
    content: Make Something Extraordinary with CodaKid Coding for Kids. 
  - image: /img/benefits/5.png
    title: Online Support from Friendly Teachers
    content: Questions or setup support, our team is here to help every step of the way!
intro:
  promo_banner:
    image: /img/banner_1.jpg
    title: Try for Free and Get Access to Dozens of Online Computer Coding Classes for Kids!
    caption: Ensure Your Child Is Not Left Behind With Digital Trends!
    stats:
      - caption: Computer Coding Courses
        figure: 33
      - caption: Online Coding Guest
        figure: 110
      - caption: Computer Coding Challenges
        figure: 440
  promo_langs:
    caption: Focus on the big picture! Jumpstart your kid's career & ensure future success by helping them 
      master the following computer coding languages!
    sub_caption: Kids Actually Find it FUN!
    images:
      - image: /img/programming/arduino.png
      - image: /img/programming/css.png
      - image: /img/programming/html.png
      - image: /img/programming/java.png
      - image: /img/programming/javascript.png
      - image: /img/programming/lua.png
      - image: /img/programming/python.png
      - image: /img/programming/unreal_engine.png
  caption: Gifts can be printed as a certificate or emailed as an e-gift.
  sub_caption: You can buy the perfect gift!
  blurbs:
    - main_image: /img/codakid_monthly_main_image.png
      title: ONLINE CODING FOR KIDS AGES 8 AND UP
      caption: CODAKID HOLIDAY SALE
      sub_caption: PICK YOUR DEAL
    - main_image: /img/yearly_main_image.png
      title: BUY 6 MONTHS
      caption: Get 6 Months Free!*
      sub_caption: Codaville All Access
    - main_image: /img/winter_title_bg.png
      title: Buy Minecraft / Roblox
      caption: Online Coding Bundles
      sub_caption: Starting at $49
  video:
    title: Get To Know Codaville
    caption: Play Video
heading: Lorem ipsum dolor sit amet
description: >-
  Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur,
  adipisci velit...
offerings:
  blurbs:
    - image: /img/coffee.png
      exerpt: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc finibus 
        sem a sem ultrices, eget sagittis magna tempor
      text: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc finibus 
        sem a sem ultrices, eget sagittis magna tempor. Quisque pulvinar lorem 
        molestie sapien ornare cursus. Praesent eget volutpat est. Proin at 
        sagittis ex. Duis quis dui magna. Nullam urna purus, blandit vitae tincidunt ut, 
        scelerisque eu sem. Etiam porttitor elit eget mi luctus, vitae blandit enim pretium. 
        Aenean nec hendrerit leo, a bibendum magna. In hac habitasse platea dictumst. 
        Suspendisse sapien magna, vestibulum non vehicula id, pellentesque in ante. Nullam 
        sed auctor tellus. Sed ipsum sem, dapibus nec eros in, feugiat sagittis mi. 
        Nullam et dui interdum, varius nibh eu, efficitur metus.
    - image: /img/coffee-gear.png
      exerpt: Fusce semper turpis sed tortor consectetur condimentum. Nulla facilisi. Nam 
        ipsum nulla, dapibus eu mi non, commodo commodo sapien
      text: Fusce semper turpis sed tortor consectetur condimentum. Nulla facilisi. Nam 
        ipsum nulla, dapibus eu mi non, commodo commodo sapien. Pellentesque luctus 
        neque id mauris accumsan, nec imperdiet justo eleifend. Nulla viverra, ipsum 
        sit amet interdum pharetra, felis lorem sollicitudin felis, vehicula finibus 
        enim nunc facilisis sapien. Donec nulla nisi, dictum quis nibh et, euismod 
        semper eros. Praesent nunc tortor, consequat eu justo ac, dictum viverra enim. 
        Etiam sed dui dapibus mauris congue facilisis. Nulla convallis, lectus vel 
        vehicula interdum, turpis nunc aliquet sem, ac iaculis ligula mauris id tortor. 
        Sed eget ornare orci, quis dignissim nulla. Pellentesque aliquam consectetur congue.
    - image: /img/tutorials.png
      exerpt: Sed in consequat leo, sit amet ullamcorper lacus. Duis lacinia, metus vitae sollicitudin 
        pharetra, ipsum augue tristique urna, in rhoncus quam tortor eget sem
      text: Sed in consequat leo, sit amet ullamcorper lacus. Duis lacinia, metus vitae sollicitudin 
        pharetra, ipsum augue tristique urna, in rhoncus quam tortor eget sem. Maecenas eu 
        pharetra orci, ut malesuada nisl. Aliquam erat volutpat. Curabitur egestas eros tincidunt, 
        scelerisque lectus ac, congue turpis. Fusce egestas sit amet elit et fringilla. Aliquam 
        erat volutpat. Vivamus ultrices venenatis maximus. Donec volutpat vitae quam at fringilla. 
        Sed luctus lacus vel tempus posuere. Ut suscipit auctor tortor. Phasellus leo dui, elementum 
        non sollicitudin eget, porta vehicula odio. Sed mollis, metus sit amet porttitor vehicula, 
        quam augue pretium erat, at commodo nisl tellus non risus.
    - image: /img/meeting-space.png
      exerpt: Vestibulum libero lectus, dignissim eget magna sit amet, malesuada tincidunt mi. Vivamus 
        sed erat iaculis mauris efficitur vehicula
      text: Vestibulum libero lectus, dignissim eget magna sit amet, malesuada tincidunt mi. Vivamus 
        sed erat iaculis mauris efficitur vehicula. Aliquam sed urna at tellus ullamcorper 
        venenatis molestie ut mi. Duis vel libero ac lectus cursus tempus. Nullam in dictum felis. 
        Nam sed laoreet turpis. Sed pretium urna consequat lorem tincidunt, ac scelerisque nisi 
        sodales. Cras tristique laoreet tempor. Mauris vitae dolor eu mauris malesuada cursus. 
        Praesent elit lectus, iaculis vel odio vitae, bibendum auctor lacus. Suspendisse potenti. 
        In tempor, massa quis euismod convallis, felis elit sodales urna, at aliquet mi elit auctor 
        risus.
testimonials:
  - author: Vaibhav Sharma
    quote: >-
      Donec scelerisque magna nec condimentum porttitor. Aliquam vel diam sed diam luctus pretium. 
      Sed quis egestas libero. Vestibulum nec venenatis ligula. 
  - author: Subarashi San
    quote: >-
      Fusce porttitor vulputate enim, nec blandit magna gravida et. Etiam et dignissim ligula. 
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
---