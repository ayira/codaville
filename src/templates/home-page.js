import React from 'react'
import PropTypes from 'prop-types'
import {graphql} from 'gatsby'
import HomePageTemplate from '../components/HomePageTemplate'

const HomePage = ({data}) => {
  const {frontmatter} = data.markdownRemark

  return (
    <HomePageTemplate
      title={frontmatter.title}
      meta_title={frontmatter.meta_title}
      meta_description={frontmatter.meta_description}
      heading={frontmatter.heading}
      description={frontmatter.description}
      offerings={frontmatter.offerings}
      testimonials={frontmatter.testimonials}
      intro={frontmatter.intro}
      promo_benefits={frontmatter.promo_benefits}
      courses_promo={frontmatter.courses_promo}
      promo_badges={frontmatter.promo_badges}
    />
  )
}

HomePage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default HomePage

export const pageQuery = graphql`
  query IndexPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        meta_title
        meta_description
        heading
        description
        offerings {
          blurbs {
            image
            text
            exerpt
          }
        }
        testimonials {
          author
          quote
        }
        promo_benefits {
          image
          title
          content
        }
        courses_promo {
          title
          courses {
            image
            caption
            sub_caption
            title
          }
        }
        promo_badges {
          image
          caption
          content
        }
        intro {
          caption
          sub_caption
          blurbs {
            main_image
            title
            caption
            sub_caption
          }
          promo_banner {
            image
            title
            caption
            stats {
              caption
              figure
            }
          }
          promo_langs {
            caption
            sub_caption
            images {
              image
            }
          }
          video {
            title
            caption
          }
        }
      }
    }
  }
`
