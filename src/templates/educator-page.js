import React from 'react'
import PropTypes from 'prop-types'
import {graphql} from 'gatsby'
import EducatorPageTemplate from '../components/EducatorPageTemplate'

const EducatorPage = ({ data }) => {
  const {frontmatter} = data.markdownRemark

  return (
    <div>
      <EducatorPageTemplate
        title={frontmatter.title}
        meta_title={frontmatter.meta_title}
        meta_description={frontmatter.meta_description}
        educator={frontmatter.educator}
      />
    </div>
  )
}

EducatorPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export const educatorPageQuery = graphql`
  query EducatorPage($id: String) {
    markdownRemark(id: {eq: $id }) {
      frontmatter {
        title
        meta_title
        meta_description
        educator {
          courses {
            image
            title
            description
          }
          feature_banner {
            image
            caption
          }
          language_banner {
            caption
            images {
              image
            }
          }
          promo_banner {
            caption
            subcaption
            image
            promo_blurbs {
              image
              icon
              title
              description
            }
          }
          inquire_banner {
            image
            caption
          }
        }
      }
    }
  }
`

export default EducatorPage
